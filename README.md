Instructions
============
Recomended for python 2.7

Using git:

    $ git clone http://github.com/galpt/galpt-LINE

Python packages:

    $ pip install thrift==0.9.3
    $ pip install rsa
    $ pip install requests

Run script:

    $ python galpt.py

Instagram
=========
gal.pt / [@gal.pt](https://www.instagram.com/gal.pt/)
